#!/usr/bin/env stack
-- stack --resolver lts-13.7 script
-- | this files, contains basic "Hello Worlds" from printing to functions
-- | also other simple things which I need to test on

main :: IO ()
main = putStrLn "Hello World"

-- | baby functions
doubleMe x = x * x

doubleUs x y = doubleMe x + doubleMe y

doubleSmallNumber x = if x > 100
    then x
    else x*2

doubleSmallNumber' x = (if x > 100 then x else x*2) + 1

concatedList = [1,2,3] ++ [4,5,6]

insertXInFront list = 'x':list

only24 = take 24 [2,24..]

-- | boomBangs takes list xs as input
-- | then for every odd number less than 10 replace with BOOM!,
-- | for every odd number bigger than 10 replace with BANG!
boomBangs xs = [ if x < 10 then "BOOM!" else "BANG!" | x <- xs, odd x]

splits [] = [([],[])]
splits (x:xs) = ([],x:xs) : [(x:ps,qs) | (ps,qs) <- splits xs]